![workflow](img/RAVAQ.png "RAVAQ logo")

# Introduction

RAVAQ is an R package implementing an integrative pipeline that (1) performs a specific quality control taking into account the case-control status to ensure data comparability, (2) selects qualifying variants as defined by the user, and (3) performs burden tests per gene.  

The pipeline integrates the tool VCFProcessor which handles VCF File and is implemented in java.  
It calls the functions QC, SplitMultiAllelic, SampleStats, recode and showfields.  
See for more details about VCFProcessor:  
http://lysine.univ-brest.fr/vcfprocessor/overview.html

The pipeline calls for the external tool PLINK (version 1.9) to perform the principal component and the relatedness analyses.   
The main arguments it uses from PLINK are *pca*, *genome*, *hardy*, *indep-pairwise*, *bmerge*, *maf*, *extract* and *keep*.   
For more details about PLINK v1.9, see https://www.cog-genomics.org/plink/1.9/.

Optionally, the pipeline proposes to run a vcf file annotation calling for the external tool VEP.   
For more details about VEP, see https://github.com/Ensembl/ensembl-vep.

The RAVAQ package calls java, python and plink through the R function system().     
For python, it needs a version 2.7.    
The python scripts were adapted from TRAPD, see https://github.com/mhguo1/TRAPD/.   

Currently, RAVAQ was tested on Linux and Mac OS. Future developments include adaptation to Windows.


![workflow](img/workflow.png "RAVAQ overall workflow")

# Installation & Tutorial

See vignette
https://gitlab.com/gmarenne/ravaq/-/blob/master/doc/RAVAQ_vignette.pdf

# Please cite

Marenne G, Ludwig TE, Bocher O, Herzig AF, Aloui C, Tournier‐Lasserve E, Génin E. RAVAQ: An integrative pipeline from quality control to region‐based rare variant association analysis. Genetic Epidemiology 2022, 46:256–265. doi: 10.1002/gepi.22450.
https://onlinelibrary.wiley.com/doi/abs/10.1002/gepi.22450
