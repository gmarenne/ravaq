#!/usr/bin/python
# This script has been adapted from https://github.com/mhguo1/TRAPD/blob/master/code/merge_snp_file.py
import optparse
import sys 

#Parse options
parser = optparse.OptionParser()
parser.add_option("-s", "--genefiles", action="store",dest="genefiles") #Comma separated list of GENE files
parser.add_option("-o", "--outfile", action="store",dest="outfilename") #Output file name 

options, args = parser.parse_args()

#Try to catch potential errors
if not options.genefiles:   # if filename is not given
    parser.error('A list of genefiles is needed')
    
if options.genefiles is not None:
    if len(options.genefiles.split(','))<2:
        parser.error('At least two genefiles are needed')
        
genefilelist=options.genefiles.split(',')
genetable={}

for f in range(0,len(genefilelist),1):
    tempfile=open(genefilelist[f], "r")
    for line_s1 in tempfile:
        line_s=line_s1.rstrip().split('\t')
        if line_s[0][0]!="#" and len(line_s)>0:
            snp=line_s[0]
            genes=line_s[1].split(',')
            if snp in genetable:
                genetable[snp][1]=genetable[snp][1]+genes
            else:
                genetable[snp]=[snp, genes]
    tempfile.close()
     
#Write Output
outfile=open(options.outfilename, "w")
outfile.write("#SNP\tGENE\n")
for x in genetable:
    if len(x)>0:
    #Read through hash table and print out variants
        gene_out=','.join(list(set(genetable[x][1])))
        outfile.write(str(x)+"\t"+gene_out+"\n")
outfile.close()
                    
                
