## This is a quick start script to run RAVAQ on the example vcf file provided with the package.
## We first split the example vcf file and the referrence panel data by chromosome to provide an example on how to run RAVAQ by chromosome in a loop.
## A more efficient strategy can be to use xargs in Linux to parallelize
 
## Please move to the directory where you want the output files to be written or specify the directory below in the object outdir
# Directory where the examples output files have to be written
outdir="./" # optional path to an output directory - default is the current directory


####################################
## INSTALLATION
####################################
# Install / Load RAVAQ
if( !("RAVAQ" %in% .packages(all.available = TRUE)) )
{
 # MAC OS users : errors converted from warnings might occured, the following command helps. Please uncomment if necessary
 #Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS=TRUE)
 library(devtools)
 devtools::install_gitlab("gmarenne/ravaq")
}
library(RAVAQ)


# CHECK ON SYSTEM REQUIREMENTS
# Default values used in ravaq()
pathpython="python"
pathjava="java"
pathPLINK="plink1.9"
loc.package=system.file(package="RAVAQ")
# Check these default values and update if necessary
system(paste0(pathjava," -version")) # Should return the version of java
system(paste0(pathpython," --version")) # Should return the version of Python 2.7
system(paste0(pathPLINK," --version")) # Should return the version of PLINK v1.9
file.exists(loc.package) # Should return TRUE
file.exists(paste0(loc.package,"/java/VCFProcessor.jar")) # Should return TRUE
file.exists(paste0(loc.package,"/python/merge_genefile.py")) # Should return TRUE
file.exists(paste0(loc.package,"/python/snp_to_genomicregion.py")) # Should return TRUE


# Install / Load Reference panel data for world-wide PCA in build 37 for the example vcf file (for RAVAQ step2)
if( !("RefPanelData.1000G.CommonSNPs.GRCh37" %in% .packages(all.available = TRUE)) )
{
 cat("Installing Reference Panel data - GRCh37..\n")
 build=37 # 37 for the example data. Build 38 data are also available, see RAVAQ vignette.
 path="http://lysine.univ-brest.fr/~gmarenne/DATA_1000G_COMMON_SNPS/RefPanelData.1000G.CommonSNPs"
 path=paste0(path,".GRCh",build,"_1.0.tar.gz")
 install.packages(path)
 rm(path)
}
library(RefPanelData.1000G.CommonSNPs.GRCh37)
# Note that these reference panel data are also available in GRCh38
# http://lysine.univ-brest.fr/~gmarenne/DATA_1000G_COMMON_SNPS/RefPanelData.1000G.CommonSNPs.GRCh38_1.0.tar.gz


####################################
## EXAMPLE DATA
####################################
vcffile = system.file("extdata","TGP_51samples_exomes.annot1gene.vcf.gz",package="RAVAQ")


####################################
## Split the example vcf file
## and the reference panel data
## per chromosome
####################################

# First split the example vcf by chromosome with VCFProcessor
outdir_splitvcf=outdir  # update if necessary to define an output directory
                        # to store the vcf files by chromosome
system(paste0(pathjava," -jar ",loc.package,"/java/VCFProcessor.jar SplitByChromosome --vcf ",
              vcffile," --gz --outdir ",outdir_splitvcf))


# Also split the reference panel data bed/bim/fam files by chromosome
# would improve efficiency, this can be done with PLINK1.9
outdir_splitrefpanel=outdir  # update if necessary to define an output directory
                             # to store the reference panel bed/bim/fam files by chromosome
for ( i in 1:22)
{
 cat(i,".. ")
 system(paste0(pathPLINK," --bfile ",refpanel_bfile," --chr ",i,
               " --make-bed --out ",outdir_splitrefpanel,
               "/chr",i,".GW_GRCh37.20130502.genotypes.common.SNPs") ,
        ignore.stdout=TRUE )
} ; cat("Done.\n")



####################################
## BASIC ARGUMENTS
####################################
# Prefix name for output files
outprefix1 = "example"
# Define sample groups through the argument groupIDlist : 2 groups of samples in this example
idline = integer(0)
skip = 0
while (length(idline) == 0)
{
 look_for_ids = utils::read.table(vcffile, as.is = T, skip = skip, nrow = 100, comment.char = "", sep = "\n")$V1
 idline = which(substr(look_for_ids, 1, 6) == "#CHROM")
 skip = skip + 100
}
ids = strsplit(look_for_ids[idline], "\t")[[1]][-c(1:9)]
groupIDlist=list( ids[which(substr(ids,1,3)=="eHG")] ,
                  ids[which(substr(ids,1,3)=="eNA")] )
lapply(groupIDlist,length)

####################################
## STEP 1 ARGUMENTS
####################################
# These example data are low depth whole genome sequencing from the 1000 Genomes project.
# We set up non-default thresholds for the genotype QC
MAX_DP=50
MIN_DP=2
MIN_GQ=10
# We use the argument removefilter_step1 in order to remove variants flagged at low VQSR tranches
# in the FILTER field of the vcf
removefilter_step1=c("VQSRTrancheSNP99.90to100.00","VQSRTrancheSNP99.00to99.90",
                     "VQSRTrancheINDEL99.00to99.90","VQSRTrancheINDEL99.90to100.00")


####################################
## STEP 2 ARGUMENTS
####################################
# Reference panel files
refpanel_directory = system.file("extdata",package="RefPanelData.1000G.CommonSNPs.GRCh37")
refpanel_bfile=paste0(refpanel_directory,"/ALL.GW_GRCh37.20130502.genotypes.common.SNPs")
data(samples.info)
colnames(samples.info)[1] = "IID"
# Build the example data and reference panel data information
example = as.data.frame( rbind(cbind(IID=groupIDlist[[1]],group="1"),
                               cbind(IID=groupIDlist[[2]],group="2")) , stringsAsFactors=F)
PCA_sampleinfo = merge(samples.info,example,by="IID",all=T) 
PCA_sampleinfo$pop[which(PCA_sampleinfo$group==1)]="Group1"
PCA_sampleinfo$pop[which(PCA_sampleinfo$group==2)]="Group2"
PCA_sampleinfo$super_pop[which(PCA_sampleinfo$group==1)]="Group1"
PCA_sampleinfo$super_pop[which(PCA_sampleinfo$group==2)]="Group2"
# Specify the target population - if not specified,
# the pipeline takes the whole reference panel as target
# here the targeted population is based on the 4 non-Finnish European 1000G populations
PCA_targetpop = PCA_sampleinfo$IID[which(PCA_sampleinfo$pop %in% c("CEU","GBR","IBS","TSI"))]
# Set up the display for the world-wide PCA
PCA_REFPANEL_display =
     data.frame( super_pop=c("AFR","AMR","EAS","EUR","SAS","Group1","Group2"),
                 col   = c("red","orange","green3","blue","purple","grey35","black"),
                 pch   = c(4,4,4,4,4,1,1),
                 order = c(3,3,3,3,3,1,2),
                 stringsAsFactors=F
               )
# Set up the display for the post sample QC PCA
PCA_STUDYpostsampleQC_display = data.frame( group=c(1,2),
                                            col = c("green3","blue"),
                                            pch = c(1,1),
                                            order = c(1,1),
                                            stringsAsFactors=F
                                          )


####################################
## STEP 4 ARGUMENTS
####################################
# In this example, BRCA1 & BRCA2 genes are discarded from the analysis
# If you don't want to discard any gene,
# leave the genenull argument to NULL in qualifarguments
brcagenes="BRCA1,BRCA2"
qualifarguments = list( RARE1=list( genecolname="GENE" ,
                                      snpformat="CHRPOSREFALT" ,
                                      includeinfo=c("1_AF[<]0.05"),
                                      genenull=brcagenes
                                    ) ,
                        RARE2=list( genecolname="GENE" ,
                                      snpformat="CHRPOSREFALT" ,
                                      includeinfo=c("2_AF[<]0.05"),
                                      genenull=brcagenes
                                    )
                      )
qualifmerge = list( RARE=c("RARE1","RARE2") )
qualifmain=c("RARE")


####################################
## STEP 5 ARGUMENTS
####################################
# In this example we ask for 3 tests
#  1 - A non-weighted burden (an individual score is the number of variants it carries)
#  2 - WSS : a weighted burden as defined by the WSS test
#  3 - The SKAT
testarguments = list( BURDENnoweight=list(test="burden",weights=NULL),
                      BURDENwss=list(test="burden",weights="WSS"),
                      SKAT=list(test="SKAT")
                    )


####################################
# Run RAVAQ step 1 by chromosome
####################################
outprefix6="exampleBYCHR"
for ( i in 1:22)
{
 ravaq( vcffile=paste0(outdir_splitvcf,"/",i,".TGP_51samples_exomes.annot1gene.vcf.gz"),
        groupIDlist=groupIDlist,
        outdir=outdir,
        outprefix=paste0(outprefix6,i),
        MAX_DP=MAX_DP, MIN_DP=MIN_DP, MIN_GQ=MIN_GQ,
        removefilter_step1=removefilter_step1,
        step=1
        #,pathjava=pathjava     #uncomment if default path value need to be changed
      )
}


####################################
# Run sampleQC_prep() to prepare files by chromosome
# Append summary in the *.RAVAQ.log files started previously when running STEP1 by chromosome
####################################
for ( i in c(1:22) )
{
 RAVAQ_title1(paste("CHR",i))
 write.table( rbind(c("START",
                      paste("RAVAQ sampleQC_prep STARTED ON",
                      format(Sys.time(),"%Y %b %d %T")))) ,
              paste0(outdir,outprefix6,i,".RAVAQ.log") ,
              quote=F,col.names=F,row.names=F,sep="\t",append=T)
 ped = as.matrix(read.table(paste0(outdir,outprefix6,i,".step1.ped"),as.is=T,
                 col.names=c("FID","IID","F","M","S","pheno","group")))
 step2a = sampleQC_prep(
     vcffile=paste0(outdir,outprefix6,i,".step1.vcf.gz"),
     ped=ped,
     refpanel_bfile=paste0(outdir_splitrefpanel,
      "/chr",i,".GW_GRCh37.20130502.genotypes.common.SNPs"),
     outdir=outdir,
     outprefix=paste0(outprefix6,i),
     pathPLINK=pathPLINK , pathjava=pathjava , loc.package=loc.package
                          )
 write.table( step2a$log ,
              paste0(outdir,outprefix6,i,".RAVAQ.log") ,
              quote=F,col.names=F,row.names=F,sep="\t",append=T)
 write.table( rbind(c("END",
                      paste("RAVAQ sampleQC_prep FINISHED ON",
                      format(Sys.time(),"%Y %b %d %T")))) ,
              paste0(outdir,outprefix6,i,".RAVAQ.log") ,
              quote=F,col.names=F,row.names=F,sep="\t",append=T)
 rm(ped,step2a)
}


####################################
# Run RAVAQ sampleQC_concat()
# to concatenate by-chromosome files generated with sampleQC_prep()
####################################
chrtodo=1:22
step2concat = sampleQC_concat(
      study_bfiles = paste0(outdir,outprefix6,chrtodo,".step2"),
      mergedrefpanel_bfiles = paste0(outdir,outprefix6,chrtodo,".step2.mergedwithrefpanel"),
      samplestats_files = paste0(outdir,outprefix6,chrtodo,".step2.samplestats.txt.gz"),
      outdir = outdir,
      outprefix = paste0(outprefix6,"all"),
      pathPLINK=pathPLINK
                                )
cat(paste(apply( step2concat$log , MARGIN=1 , FUN=paste , collapse=" : " ),collapse="\n"),"\n")

####################################
# Run RAVAQ sampleQC_main() to  perform the sample QC
# Note : a ped matrix is needed, the chromosome 1 ped file is used
# (all chromosome ped files are the same)
####################################
ped = as.matrix(read.table(paste0(outdir,outprefix6,1,".step1.ped"),as.is=T,
                col.names=c("FID","IID","F","M","S","pheno","group")))
set.seed(1000)
step2b = sampleQC_main( study_bfile = step2concat$study_bfile,
                        mergedrefpanel_bfile  = step2concat$mergedrefpanel_bfile,
                        samplestats_file = step2concat$samplestats_file,
                        ped = ped,
                        outdir=outdir,
                        outprefix=paste0(outprefix6,"all"),
                        PCA_sampleinfo=PCA_sampleinfo,
                        PCA_targetpop=PCA_targetpop,
                        PCA_REFPANEL_display=PCA_REFPANEL_display,
                        PCA_STUDYpostsampleQC_display=PCA_STUDYpostsampleQC_display,
                        geneticmap=HumanGeneticMap::genetic.map.b37,
                        pathPLINK=pathPLINK
                      )
cat(paste(apply( step2b$log , MARGIN=1 , FUN=paste , collapse=" : " ),collapse="\n"),"\n")

####################################
# QC and define an updated groupIDlist
####################################
load(paste0(outdir,outprefix6,"all.step2.sampleQC.RData"))
dim(sampleQC)
table(sampleQC$Exclude,sampleQC$group) # 11 individuals were flagged to be excluded
groupIDlistPOSTQC = list( sampleQC$IID[which(sampleQC$group==1 & sampleQC$Exclude=="")] ,
                          sampleQC$IID[which(sampleQC$group==2 & sampleQC$Exclude=="")] )
lapply(groupIDlistPOSTQC,length)

####################################
# Run RAVAQ steps 3 to 5 by chromosome
# (in a for loop again, see below for the Linux xargs alternative)
# the main function ravaq() needs a groupIDlist
# it needs to be updated according to the sample QC performed across all chromosomes
####################################
groupIDlistPOSTQC = tapply( step2b$postQCped[,"IID"] ,
                            INDEX=as.factor(step2b$postQCped[,"group"]) ,
                            FUN=identity )
for ( i in 1:22)
{
 RAVAQ_title1(paste("CHR",i))
 ravaq( vcffile=paste0(outdir,outprefix6,i,".step1.vcf.gz"),
        groupIDlist=groupIDlistPOSTQC,
        outdir=outdir,
        outprefix=paste0(outprefix6,i),
        steps=3:5,
        MAX_DP=MAX_DP, MIN_DP=MIN_DP, MIN_GQ=MIN_GQ,
        qualifarguments=qualifarguments,
        qualifmerge=qualifmerge,
        qualifmain=qualifmain,
        testarguments=testarguments
        #,pathpython=pathpython #uncomment if default path value need to be changed
        #,pathjava=pathjava     #uncomment if default path value need to be changed
        #,pathPLINK             #uncomment if default path value need to be changed
      )
}

####################################
# Run an additionnal function to concatenate by-chromosome step 5 results
####################################
chrtodo=1:22
step5concat = regionAssoc_concat(
       summary_files = paste0(outdir,outprefix6,chrtodo,".step5.summary.RData"),
       test_files = paste0(outdir,outprefix6,chrtodo,".step5.test.RData"),
       TOPgenotypesofinterest_files = paste0(outdir,
                                             outprefix6,
                                             chrtodo,
                                             ".step5.TOPgenotypesofinterest.RData"),
       outdir=outdir,
       outprefix=paste0(outprefix6,"all")
                                )
cat(paste(apply(step5concat$log,MARGIN=1,FUN=paste,collapse=" : "),collapse="\n"),"\n")




