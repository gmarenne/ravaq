alleleQC = function( vcffile , ped , MIN_GQ="disabled" , MIN_DP="disabled" , MAX_DP="disabled" , MAX_AB_GENO_DEV="disabled" , MIN_QD=2 , MIN_INBREEDING=-0.8 , MIN_MQRANKSUM=-12.5 , MAX_FS_INDEL=200 , MAX_FS_SNP=60 , MAX_SOR_INDEL=10 , MAX_SOR_SNP=3 , MIN_MQ_INDEL=10 , MIN_MQ_SNP=40 , MIN_RPRS_INDEL=-20 , MIN_RPRS_SNP=-8 , MAX_ABHET_DEV=0.25 , MIN_CALLRATE=0.9 , MIN_FISHER_CALLRATE=0.001 , removefilter_step3=NULL , keepfilter_step3=NULL , veparguments=NULL , outdir=NULL , outprefix="out" , pathjava="java" , loc.package=system.file(package="RAVAQ") , verbose=TRUE ,check=TRUE )
{
 if(check)
 {
  if( is.null(outdir) ){ outdir=getwd() }
  if(strsplit(outdir,"")[[1]][nchar(outdir)]!="/") {outdir=paste0(outdir,"/")}
  if( !file.exists(outdir) ) { stop(paste("The outdir directory does not exist:\n",  outdir,"\n")) }
  if (!file.exists(vcffile)) { stop(paste("The vcf file does not exist:\n  ",vcffile,"\n")) }
  if( !is.matrix(ped) ){ stop("The argument ped is not a matrix.\n") } else { if(ncol(ped)!=7 | !("group" %in% colnames(ped))) {stop("The ped matrix must have 7 columns, the seventh must be the group.\n")} }
if( !file.exists(loc.package) | loc.package=="" ) { stop(paste("The package location directory does not exist:\n",  loc.package,"\n")) }
 }

 pipelinesummary = rbind(   c( "STEP" , paste("RAVAQ STEP3 STARTED ON",format(Sys.time(), "%Y %b %d %T")) )
                          , c( "INPUT" , paste0("Input vcf file : ",vcffile) )
                          , c( "SUMMARY" , paste0("Number of samples in ped matrix : ",nrow(ped)) )
                          , c( "SUMMARY" , paste0("Number of groups in ped matrix : ",length(unique(ped[,"group"]))) )
                        )

 if (verbose) { RAVAQ_title2("Split multi-allelic variants") }
 system( paste0(pathjava," -jar ",loc.package,"/java/VCFProcessor.jar SplitMultiAllelic --vcf ",vcffile," --out ",outdir,outprefix,".step3.split.vcf.gz --gz 2> ",outdir,outprefix,".step3.split.log") )# --log ",outdir,outprefix,".step3.split.log") )
 
 if (verbose) { RAVAQ_title2("Run allele QC") }
 paramQC = c( MIN_GQ=MIN_GQ,
              MIN_DP=MIN_DP,
              MAX_DP=MAX_DP,
              MAX_AB_GENO_DEV=MAX_AB_GENO_DEV,
              MIN_QD=MIN_QD,
              MIN_INBREEDING=MIN_INBREEDING,
              MIN_MQRANKSUM=MIN_MQRANKSUM,
              MAX_FS_INDEL=MAX_FS_INDEL,
              MAX_FS_SNP=MAX_FS_SNP,
              MAX_SOR_INDEL=MAX_SOR_INDEL,
              MAX_SOR_SNP=MAX_SOR_SNP,
              MIN_MQ_INDEL=MIN_MQ_INDEL,
              MIN_MQ_SNP=MIN_MQ_SNP,
              MIN_RPRS_INDEL=MIN_RPRS_INDEL,
              MIN_RPRS_SNP=MIN_RPRS_SNP,
              MAX_ABHET_DEV=MAX_ABHET_DEV,
              MIN_CALLRATE=MIN_CALLRATE,
              MIN_FISHER_CALLRATE=MIN_FISHER_CALLRATE
            )
 utils::write.table(cbind(paste(names(paramQC),paramQC,sep="\t")),paste0(outdir,outprefix,".step3.param.qc.tsv"),quote=F,col.names=F,row.names=F)
 utils::write.table(ped,paste0(outdir,outprefix,".step3.ped"),quote=F,col.names=F,row.names=F)
 cmd1 = paste0(pathjava," -jar ",loc.package,"/java/VCFProcessor.jar QC --vcf ",outdir,outprefix,".step3.split.vcf.gz --ped ",outdir,outprefix,".step3.ped --opt ",outdir,outprefix,".step3.param.qc.tsv --report ",outdir,outprefix,".step3.filteredVariant.tsv.gz --out ",outdir,outprefix,".step3.qced.vcf.gz") # --log ",outdir,outprefix,".step3.qc.log")
 if (!is.null(removefilter_step3)) { cmd1 = paste0(cmd1," --remove-filtered-any ",paste(removefilter_step3,collapse=",")) }
 if (!is.null(keepfilter_step3)) { cmd1 = paste0(cmd1," --keep-filtered-any ",paste(keepfilter_step3,collapse=",")) }
 cmd1 = paste0(cmd1," --gz 2> ",outdir,outprefix,".step3.qc.log")
 system(cmd1) ; rm(cmd1)
 
 if(!is.null(veparguments))
 {
  if (verbose) { RAVAQ_title2("VEP") }
  do.call( RunVEP , c( list( input=paste0(outdir,outprefix,".step3.qced.vcf.gz") , output=paste0(outdir,outprefix,".step3.vep.vcf") ) , veparguments ) )
  outvcffile=paste0(outdir,outprefix,".step3.vep.vcf.gz")
 } else { outvcffile=paste0(outdir,outprefix,".step3.qced.vcf.gz")}

 pipelinesummary = rbind(  pipelinesummary
                          , c( "INPUT" , paste0("Non default variant QC parameters detailled in ",outdir,outprefix,".step3.param.qc.tsv") )
                          , c( "SUMMARY" , paste0("Number of variants in STEP3 input vcf file : ",strsplit(strsplit(system(paste0("grep \"Variant Kept\" ",outdir,outprefix,".step3.split.log"),intern=T)," ")[[1]][7],"/")[[1]][2]))
                          , c( "SUMMARY" , paste0("Number of alleles after splitting multi-allelic variants : ",strsplit(strsplit(system(paste0("grep \"Variant Kept\" ",outdir,outprefix,".step3.qc.log"),intern=T)," ")[[1]][7],"/")[[1]][2]))
                          , c( "SUMMARY" , paste0("Number of QCed alleles (STEP3) : ",utils::tail(strsplit(system(paste0("grep -F \"Number of Variants [Unfiltered\" ",outdir,outprefix,".step3.qc.log"),intern=T)," ")[[1]],n=1)))
                          , unlist(ifelse(!is.null(veparguments), yes=list(c("SUMMARY","The vcf has been annotated with VEP")) , no=list(NULL) ))
                          , c( "OUTPUT" , paste0("For the list of alleles excluded by the allele QC see : ",outdir,outprefix,".step3.filteredVariant.tsv.gz") )
                          , c( "OUTPUT" , paste0("STEP3 vcf file : ",outvcffile) )
                        )

 if (verbose) { RAVAQ_title2("STEP 3 DONE") }
 return(list( outvcffile = outvcffile ,
              log=pipelinesummary
            )
       )
}

